﻿using System;

namespace WorkWithDB.DAL.Abstract
{
    public interface ITransactionManager : IDisposable
    {
        void Commit();
        void RollBack();
    }
}