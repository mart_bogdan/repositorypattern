﻿using System.Configuration;
using System.Data.SqlClient;
using WorkWithDB.DAL.Abstract;

namespace WorkWithDB.DAL.SqlServer
{
    public class TransactionManager : ITransactionManager
    {
        private readonly SqlTransaction _transaction;
        private readonly SqlConnection _connection;

        public TransactionManager()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction(/*IsolationLevel.ReadCommitted*/);
        }
        public void Dispose()
        {
            try
            {
                if (_transaction != null) _transaction.Dispose();
            }
            finally
            {
                _connection.Dispose();
            }
        }

        public void Commit()
        {
            if (_transaction != null) _transaction.Commit();
        }

        public void RollBack()
        {
            if (_transaction != null) _transaction.Rollback();
        }
    }
}