﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using WorkWithDB.DAL.Abstract;
using WorkWithDB.DAL.SqlServer.Repository;

namespace WorkWithDB.DAL.SqlServer
{
    public class SqlServerDiInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<ITransactionManager>().ImplementedBy<TransactionManager>().LifestylePerWebRequest());
            container.Register(Component.For<IAuthRepository>().ImplementedBy<AuthRepository>().LifestyleTransient());
            container.Register(Component.For<IBlogPostRepository>().ImplementedBy<BlogPostRepository>().LifestyleTransient());
            container.Register(Component.For<IBlogUserRepository>().ImplementedBy<BlogUserRepository>().LifestyleTransient());
        }
    }
}